package com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities

data class NearbySearchRequestResults(
    val results : List<Place>
    )
{}