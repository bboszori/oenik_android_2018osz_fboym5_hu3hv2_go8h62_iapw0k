package com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities

data class Place(
    val geometry : Geometry,
    val name : String?,
    val rating : Double?,
    val reference: String,
    val vicinity : String?,
    var url : String? = null
)