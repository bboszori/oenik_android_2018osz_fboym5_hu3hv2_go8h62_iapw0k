package com.oenik.androidfejl.giftadvisor

import android.os.Parcel
import android.os.Parcelable

data class Interest( val keyword : String, val showOnMap : Boolean) : Parcelable
{
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    constructor(text : String) : this(text, true) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(keyword)
        parcel.writeByte(if (showOnMap) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Interest> {
        override fun createFromParcel(parcel: Parcel): Interest {
            return Interest(parcel)
        }

        override fun newArray(size: Int): Array<Interest?> {
            return arrayOfNulls(size)
        }
    }
}