package com.oenik.androidfejl.giftadvisor

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build.ID
import android.os.Parcelable
import android.util.Log
import android.widget.Toast

val DATABASE_NAME = "GiftAdvisorDB"
val CONTACTS_TNAME = "Contacts"
val COL_ID = "contactID"
val COL_NAME = "name"
val COL_BIRTHYEAR = "birthyear"
val COL_BIRTHMONTH = "birthmonth"
val COL_BIRTHDAY = "birthday"
val COL_PHOTO = "photo"
val INTRESTS_TNAME = "Interests"
val COL_IID = "interestID"
val COL_CID = "contactID"
val COL_INAME = "interestname"
val COL_SHOW = "showOnMap"

class DataBaseHadndler private constructor(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1){



    init {
        ++myInstancesCount
    }

    companion object {
        var myInstancesCount = 0;
        private var sInstance: DataBaseHadndler? = null

        @Synchronized
        fun getInstance(context: Context): DataBaseHadndler {
            if (sInstance == null) {
                sInstance = DataBaseHadndler(context.applicationContext)
            }
            return sInstance as DataBaseHadndler
        }
    }



    override fun onConfigure(db: SQLiteDatabase) {
        super.onConfigure(db)
        db.setForeignKeyConstraintsEnabled(true)
    }


    override fun onCreate(db: SQLiteDatabase?) {
        val createContactTable = "CREATE TABLE " + CONTACTS_TNAME + " (" +
                            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            COL_NAME + " VARCHAR(256), " +
                            COL_BIRTHYEAR + " INTEGER, " +
                            COL_BIRTHMONTH + " INTEGER, " +
                            COL_BIRTHDAY + " INTEGER);";

        val createIntrestTable = "CREATE TABLE " + INTRESTS_TNAME + " (" +
                COL_IID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_CID + " INTEGER, " +
                COL_INAME + " VARCHAR(256), " +
                COL_SHOW + " INTEGER DEFAULT 0, " +
                " FOREIGN KEY ("+COL_CID+") REFERENCES "+ CONTACTS_TNAME +"(" + COL_ID + "));";

        db?.execSQL(createContactTable)
        db?.execSQL(createIntrestTable)


    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + INTRESTS_TNAME);
        db?.execSQL("DROP TABLE IF EXISTS " + CONTACTS_TNAME);
        onCreate(db);
    }

    fun insertContactData(contact : Contact){
        val db = this.writableDatabase
        var cv = ContentValues()

        cv.put(COL_NAME, contact.contactName)
        cv.put(COL_BIRTHYEAR, contact.birthYear)
        cv.put(COL_BIRTHMONTH, contact.birthMonth)
        cv.put(COL_BIRTHDAY, contact.birthDay)

        var result = db.insert(CONTACTS_TNAME, null, cv)


        if (result == -1.toLong()){
            Toast.makeText(context, "Failed to insert", Toast.LENGTH_SHORT).show()
        } else {
            var id = result.toLong()
            if (contact.interests.size > 0){
                contact.interests.forEach{
                    var show = if (it.showOnMap) 1 else 0
                    var intcv = ContentValues()
                    intcv.put(COL_INAME, it.keyword)
                    intcv.put(COL_CID, id)
                    intcv.put(COL_SHOW, show)

                    db.insert(INTRESTS_TNAME, null, intcv)
                }
            }

            Toast.makeText(context, "Succeeded to insert", Toast.LENGTH_SHORT).show()
        }
        db.close()
    }

    fun getAllContacts(): ArrayList<Contact> {
        val contacts = ArrayList<Contact>()
        val db = this.writableDatabase
        val selectQuery = "SELECT  * FROM " + CONTACTS_TNAME
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.count != 0) {
            cursor.moveToFirst()
            while (cursor.moveToNext()) {
                val contact = Contact(
                    cursor.getString(cursor.getColumnIndex(COL_NAME)),
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_BIRTHYEAR))),
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_BIRTHMONTH))),
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_BIRTHDAY)))

                )

                contact.contactID = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_ID)))

                val interestQuery = "SELECT * FROM $INTRESTS_TNAME WHERE $COL_CID = ${contact.contactID}"
                val icursor = db.rawQuery(interestQuery, null)
                if (icursor.count != 0) {
                    icursor.moveToFirst()
                    while (icursor.moveToNext())
                    {
                        val interest = Interest(
                            icursor.getString(icursor.getColumnIndex(COL_INAME)),
                            (icursor.getInt(icursor.getColumnIndex(COL_SHOW)) == 1)
                        )
                        contact.interests.add(interest)
                    }

                }
                icursor.close()


                contacts.add(contact)
            }
        }
        cursor.close()
        db.close()
        return contacts
    }

    fun searchCOntactByName(name: String) : ArrayList<Contact>{
        val contacts = ArrayList<Contact>()
        val db = this.writableDatabase
        val selectQuery = "SELECT * FROM $CONTACTS_TNAME WHERE $COL_NAME = '$name'"
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor != null) {
            cursor.moveToFirst()
            while (cursor.moveToNext()) {
                val contact = Contact(
                    cursor.getString(cursor.getColumnIndex(COL_NAME)),
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_BIRTHYEAR))),
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_BIRTHMONTH))),
                    Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_BIRTHDAY)))

                )

                contact.contactID = Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_ID)))

                val interestQuery = "SELECT * FROM $INTRESTS_TNAME WHERE $COL_CID = ${contact.contactID}"
                val icursor = db.rawQuery(interestQuery, null)
                if (icursor != null) {
                    icursor.moveToFirst()
                    while (icursor.moveToNext())
                    {
                        val interest = Interest(
                            icursor.getString(icursor.getColumnIndex(COL_INAME)),
                            (icursor.getInt(icursor.getColumnIndex(COL_SHOW)) == 1)
                        )
                        contact.interests.add(interest)
                    }

                }
                icursor.close()


                contacts.add(contact)
            }
        }
        cursor.close()
        db.close()
        return contacts
    }

    fun updateContact(contact : Contact) {
        val db = this.writableDatabase
        var values = ContentValues()
        values.put(COL_ID, contact.contactID)
        values.put(COL_NAME, contact.contactName)
        values.put(COL_BIRTHYEAR, contact.birthYear)
        values.put(COL_BIRTHMONTH, contact.birthMonth)
        values.put(COL_BIRTHDAY, contact.birthDay)

        val ret = db.update(CONTACTS_TNAME, values, "$COL_ID = '${contact.contactID}'", null)
            if (contact.interests.size > 0){
                contact.interests.forEach{
                    val interestQuery = "SELECT * FROM $INTRESTS_TNAME WHERE $COL_CID = ${contact.contactID} AND $COL_INAME = '${it.keyword}'"
                    val cursor = db.rawQuery(interestQuery, null)
                    val num = cursor.count
                    if (cursor.count <= 0){
                        var show = if (it.showOnMap) 1 else 0
                        var intcv = ContentValues()
                        intcv.put(COL_INAME, it.keyword)
                        intcv.put(COL_CID, contact.contactID)
                        intcv.put(COL_SHOW, show)
                        val _success = db.insert(INTRESTS_TNAME, null, intcv)
                        if (_success.toInt() != -1){{
                            Toast.makeText(context, "Interest added", Toast.LENGTH_SHORT)
                        }
                        }

                    }
                }

            }

        db.close()

    }

    fun insertOrupdateContact(contact : Contact)
    {
        val contacts = ArrayList<Contact>()
        val name : String = contact.contactName
        val db = this.writableDatabase
        val selectQuery = "SELECT * FROM $CONTACTS_TNAME WHERE $COL_NAME = '$name'"
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.count != 0) {
            updateContact(contact)
            Toast.makeText(context, "Contact updated", Toast.LENGTH_SHORT).show()
        }
        else{
            insertContactData(contact)
            Toast.makeText(context, "Contact saved", Toast.LENGTH_SHORT).show()
        }
    }

    fun deleteContact(contact: Contact) {

        val db = this.writableDatabase
        db.delete(INTRESTS_TNAME, "$COL_CID = ${contact.contactID}", null)

        val retVal = db.delete(CONTACTS_TNAME, "$COL_ID = ${contact.contactID}", null)

        if (retVal >= 1) {
            Log.v("@@@WWe", " Record deleted")
        } else {
            Log.v("@@@WWe", " Not deleted")
        }
        db.close()

    }

}
