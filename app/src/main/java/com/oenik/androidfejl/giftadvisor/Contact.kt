package com.oenik.androidfejl.giftadvisor

import android.os.Parcel
import android.os.Parcelable
import org.threeten.bp.LocalDate
import org.threeten.bp.temporal.ChronoUnit
import java.lang.Exception
//import com.oenik.androidfejl.giftadvisor.R.id.name
//import com.oenik.androidfejl.giftadvisor.R.id.birthdate
import kotlin.collections.ArrayList

data class Contact(
    var contactID: Int,
    val contactName: String,
    var birthYear: Int,
    var birthMonth: Int,
    var birthDay: Int,
    val interests: ArrayList<Interest>,
    var photo : ByteArray? = null,
    var selected: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        arrayListOf<Interest>().apply {
        parcel.readList(this,Interest::class.java.classLoader)
        },

        parcel.createByteArray(),
        selected = parcel.readInt()==1) {}

    constructor(contactName : String, birthYear : Int = 0, birthMonth : Int = 0, birthDay : Int = 0, photo : ByteArray? = null,selected: Boolean = false) : this(
        0,
        contactName,
        birthYear,
        birthMonth,
        birthDay,
        ArrayList<Interest>(),
        photo,
        selected

    ){}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(contactID)
        parcel.writeString(contactName)
        parcel.writeInt(birthYear)
        parcel.writeInt(birthMonth)
        parcel.writeInt(birthDay)
        //parcel.createTypedArrayList<Interest>(Interest.CREATOR)
        parcel.writeList(interests)
        parcel.writeByteArray(photo)
        parcel.writeInt(if (selected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Contact> {
        override fun createFromParcel(parcel: Parcel): Contact {
            return Contact(parcel)
        }

        override fun newArray(size: Int): Array<Contact?> {
            return arrayOfNulls(size)
        }

        fun getBirthdayString(curr: Contact) : String{
            var outStr = ""
            try {
                outStr = LocalDate.of(curr.birthYear, curr.birthMonth, curr.birthDay).toString()
            }
            catch (e : Exception){}
            return outStr
        }


        fun getAgeInDaysString(curr : Contact) : String{
            // if stored date is invalid, it throws an exception..

            var nextAge : Long = 0
            var daysUntil : Long = 0

            try {
                val birthday = LocalDate.of(curr.birthYear, curr.birthMonth, curr.birthDay)
                val now = LocalDate.now()
                val birthdayThisYear = LocalDate.of(now.year, birthday.month, birthday.dayOfMonth)
                val daysbetween = ChronoUnit.DAYS.between(now,birthdayThisYear)

                // if 'daysbetween' is negative, it means, we already had a birthday this year,
                // so the next one will be in the next year, hence the +1
                var nextBirthday : LocalDate? = null
                if (daysbetween<0){
                    nextBirthday = LocalDate.of(now.year + 1, birthday.month, birthday.dayOfMonth)
                }
                else{
                    // if non-negative(0 or positive) either the next birthday is today, or later this year (so year = now.year)
                    nextBirthday = LocalDate.of(now.year,birthday.month, birthday.dayOfMonth)
                }
                nextAge = ChronoUnit.YEARS.between(birthday,nextBirthday)
                daysUntil = ChronoUnit.DAYS.between(now, nextBirthday)
            }
            catch (e : Exception){

            }

            // if the given birthday is valid (nextAge didn't remain 0), so no exception was thrown,
            // it can return the "Age X in Y days" string
            if (nextAge != 0L){
                return "Age $nextAge in $daysUntil days"
            }
            else{
                // else it should return nothing (an empty string)
                return ""
            }
        }


    }
}