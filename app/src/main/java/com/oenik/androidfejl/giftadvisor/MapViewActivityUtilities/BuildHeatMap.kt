package com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.TileOverlayOptions
import com.google.maps.android.heatmaps.HeatmapTileProvider
import java.io.File
import java.util.*

/*
* Singleton that builds a heatmap that can be added to a Google Maps view.
*/
object BuildHeatMap
{
    private var heatmapTileOverlay : TileOverlayOptions? = null
    private lateinit var lastOverlayUpdate : Calendar
    val lockObject : Any = Any()
    lateinit var heatMapDataFilePath : String

    private val millisInADay = 86400000


    fun getHetMapTileOverlay() : TileOverlayOptions?
    {
        if (heatmapTileOverlay == null ||
            (Calendar.getInstance().timeInMillis - lastOverlayUpdate.timeInMillis > millisInADay ))
        {
            buildHeatmapTileOverlay()
        }
        return heatmapTileOverlay
    }

    private fun buildHeatmapTileOverlay() {
        // get ArrayList of LatLng-s from the storage file
        val heatMapDataPoints: ArrayList<LatLng> = ArrayList()

        // TODO: Make sure this is the right path
        try {
            synchronized(lockObject)
            {
                File(heatMapDataFilePath).forEachLine {
                    val latLngPairStringList = it.split(',')
                    val parsedLatLng = LatLng(latLngPairStringList[0].toDouble(), latLngPairStringList[1].toDouble())
                    heatMapDataPoints.add(parsedLatLng)
                }
            }
        } catch (e: Exception) {
            // Error handling?
        }
        if (!heatMapDataPoints.isEmpty())
        {
            heatmapTileOverlay = TileOverlayOptions()
                .tileProvider(
                    HeatmapTileProvider.Builder()
                        .data(heatMapDataPoints)
                        .opacity(0.5)
                        .build()
                )
            lastOverlayUpdate = Calendar.getInstance()
        }
    }

}