package com.oenik.androidfejl.giftadvisor

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.util.SparseBooleanArray
import android.databinding.adapters.CompoundButtonBindingAdapter.setChecked
import android.widget.CheckBox
import android.widget.Toast
import android.support.design.widget.CoordinatorLayout.Behavior.setTag
import android.graphics.BitmapFactory
import android.graphics.Bitmap






class ContactAdapter(private val context: Context, private val contactArrayList: ArrayList<Contact>) : BaseAdapter(){

    var itemStateArray = SparseBooleanArray()

    override fun getViewTypeCount(): Int {
        return count
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getCount(): Int {
        return contactArrayList.size
    }

    override fun getItem(position: Int): Any {
        return contactArrayList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder : ViewHolder

        if (convertView == null){
            holder = ViewHolder()
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.contact_lv_item,null,true)

            holder.tvName = convertView!!.findViewById(R.id.name) as TextView
            holder.tvbd = convertView.findViewById(R.id.bd) as TextView
            holder.tvPhoto = convertView!!.findViewById(R.id.img_contact) as ImageView
            holder.tvCb = convertView!!.findViewById(R.id.checkbox) as CheckBox

            convertView.tag = holder
        }
        else{
            holder = convertView.tag as ViewHolder
        }

        val bdString : String = contactArrayList[position].birthYear.toString() + "-" +
                contactArrayList[position].birthMonth.toString() + "-" +
                contactArrayList[position].birthDay.toString()

        if (bdString == "0-0-0"){
            holder.tvbd!!.setText("")
        }
        else{
            holder.tvbd!!.setText(bdString)
        }

        holder.tvName!!.setText(contactArrayList[position].contactName)
        val ba :ByteArray? = contactArrayList[position].photo
        if (ba != null){
            val bmp = BitmapFactory.decodeByteArray(contactArrayList[position].photo, 0, contactArrayList[position].photo!!.size)
            holder.tvPhoto!!.setImageBitmap(bmp)
        }


        holder.tvCb!!.isChecked = contactArrayList[position].selected
        holder.tvCb!!.setOnClickListener{
            contactArrayList[position].selected = !contactArrayList[position].selected
        }


        return convertView
    }


    private inner class ViewHolder{
        var tvName : TextView? = null
        var tvbd : TextView? = null
        var tvPhoto : ImageView? = null
        var tvCb : CheckBox? = null
    }
}