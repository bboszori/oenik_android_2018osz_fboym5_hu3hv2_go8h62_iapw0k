package com.oenik.androidfejl.giftadvisor

import android.Manifest
import android.app.Application
import android.content.ContentResolver
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.ListView
import android.database.Cursor
import android.util.Log
import kotlinx.android.synthetic.main.contact_lv_item.*
import android.net.Uri.withAppendedPath
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import java.io.ByteArrayInputStream
import java.io.InputStream
import android.provider.ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY
import android.graphics.Bitmap
import android.support.annotation.RequiresApi
import android.widget.ImageView
import java.lang.Exception
import android.provider.ContactsContract.CommonDataKinds
import android.util.SparseBooleanArray
import android.widget.Button
import android.widget.Toast
import java.io.ByteArrayOutputStream
import java.security.AccessController.getContext


class ContactsActivity : AppCompatActivity() {

    private var listView: ListView? = null
    private var contactAdapter: ContactAdapter? = null
    private var contactArrayList: ArrayList<Contact>? = null



    private val PERMISSIONS_REQUEST_READ_CONTACTS = 100

    var dbHandler: DataBaseHadndler? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)

        listView = findViewById(R.id.listView) as ListView
        contactArrayList = ArrayList()
        dbHandler = DataBaseHadndler.getInstance(applicationContext)
        val saveBtn: Button = findViewById<Button>(R.id.savebtn)
        saveBtn.setOnClickListener {
                for (i in 0..contactArrayList!!.size -1){

                    if ( contactArrayList!![i].selected){
                        dbHandler!!.insertOrupdateContact(contactArrayList!![i])

                    }

                    val intent = Intent(it.context, HomescreenActivity::class.java)
                    startActivity(intent)

                }
        }

        val cancelBtn: Button = findViewById<Button>(R.id.cancelbtn)
        cancelBtn.setOnClickListener{
            val intent = Intent(it.context, HomescreenActivity::class.java)
            startActivity(intent)
        }

        if (checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PERMISSIONS_REQUEST_READ_CONTACTS)
            //callback onRequestPermissionsResult
        } else {
            loadContacts()
        }
    }



    @RequiresApi(Build.VERSION_CODES.ECLAIR)
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadContacts()

            } else {
                Toast.makeText(this, "Permission must be granted in order to display contacts information", Toast.LENGTH_LONG)

            }
        }
    }


    fun loadContacts() {
        val phones = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        )




        while (phones!!.moveToNext()) {
            val name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            val cid =
                phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Event.CONTACT_ID)).toLong()

            val bd = contentResolver
            val bdc = bd.query(
                ContactsContract.Data.CONTENT_URI,
                arrayOf(ContactsContract.CommonDataKinds.Event.DATA),
                ContactsContract.Data.CONTACT_ID + " = " + cid
                        + " AND " + ContactsContract.Contacts.Data.MIMETYPE + " = '"
                        + ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE + "' AND "
                        + ContactsContract.CommonDataKinds.Event.TYPE + " = "
                        + ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY,
                null,
                ContactsContract.Data.DISPLAY_NAME
            )

            var birthday = ""

            val bDayColumn = bdc.getColumnIndex(ContactsContract.CommonDataKinds.Event.START_DATE)
            if (bdc!!.count > 0) {
                while (bdc.moveToNext()) {
                    birthday = bdc.getString(bDayColumn)
                }
            }

            val contact = Contact(name)

            if (birthday.isNotEmpty()){
                contact.birthYear = birthday.substring(0,4).toInt()
                contact.birthMonth = birthday.substring(5,7).toInt()
                contact.birthDay = birthday.substring(8).toInt()
            }





            var photo: Bitmap? = null

            try {
                val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(
                    contentResolver,
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, cid.toLong())
                )

                if (inputStream != null) {
                    photo = BitmapFactory.decodeStream(inputStream)
                    val stream = ByteArrayOutputStream()
                    photo.compress(Bitmap.CompressFormat.PNG, 90, stream)
                    val image = stream.toByteArray()
                    contact.photo = image
                }
                assert(inputStream != null)
                inputStream!!.close()

            } catch (e: Exception) {
                e.printStackTrace()
            }

            contactArrayList!!.add(contact)
            Log.d("name>>", name)
        }
        phones.close()

        contactAdapter = ContactAdapter(this, contactArrayList!!)
        listView!!.adapter = contactAdapter
    }

}