package com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities

data class Geometry(
    val location: Location
    )