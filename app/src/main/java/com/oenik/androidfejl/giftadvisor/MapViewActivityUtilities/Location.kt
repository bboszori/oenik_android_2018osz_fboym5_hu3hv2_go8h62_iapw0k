package com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities

data class Location(
    val lat : Double,
    val lng : Double
    )