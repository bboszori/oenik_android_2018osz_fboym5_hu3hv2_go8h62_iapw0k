package com.oenik.androidfejl.giftadvisor

import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.oenik.androidfejl.giftadvisor.databinding.EfRecyclerviewItemBinding
import kotlinx.android.synthetic.main.activity_edit_friend.*

class EditFriendActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: LinearLayoutManager
    private lateinit var currentContact: Contact

    lateinit var edittext : EditText




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_friend)

        val data = intent.extras
        currentContact = data?.getParcelable("contact")!!

        setHeader()
        initRecyclerView()
        setFloatingActionButtonListeners()

        val dbHandler : DataBaseHadndler = DataBaseHadndler.getInstance(applicationContext)

        edittext = findViewById(R.id.newitem) as EditText
        addFab.setOnClickListener{view ->
            var newin = edittext.text.toString()
            var newinterest : Interest = Interest(newin)
            currentContact.interests.add(newinterest)
            edittext.setText("")
            initRecyclerView()
        }

        editFab.setOnClickListener{view ->
            dbHandler.updateContact(currentContact)
            val intent = Intent(view.context, HomescreenActivity::class.java)
            startActivity(intent)
        }

    }


    private fun setFloatingActionButtonListeners() {



    }

    private fun initRecyclerView() : Unit {
        viewManager = LinearLayoutManager(this)
        viewAdapter = EFContactAdapter(currentContact.interests)

        recyclerView = findViewById<RecyclerView>(R.id.ef_recyclerview).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter
            adapter = viewAdapter
        }

    }

    private fun setHeader() : Unit{


        val linearlayout = findViewById<LinearLayout>(R.id.hs_linearlayout)

        // setting the background color
        //linearlayout.setBackgroundResource(R.color.color1)

        // getting references to each TextView
        val name = linearlayout.findViewById<TextView>(R.id.name)
        val birthday = linearlayout.findViewById<TextView>(R.id.birthday)
        val age = linearlayout.findViewById<TextView>(R.id.age)

        // setting the TextViews' text
        name.text = currentContact.contactName
        birthday.text = Contact.getBirthdayString(currentContact)
        age.text = Contact.getAgeInDaysString(currentContact)

        // setting the TextView's textSize
        name.textSize = 15f
        birthday.textSize = 15f
        age.textSize = 15f
    }
}
