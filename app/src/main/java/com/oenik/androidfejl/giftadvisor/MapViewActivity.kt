package com.oenik.androidfejl.giftadvisor

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.TileOverlay
import com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities.BuildHeatMap
import com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities.GiftAdvisorInfoWindowsAdapter
import com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities.NearbySearchRequestResults
import com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities.Place
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.util.concurrent.ConcurrentHashMap

class MapViewActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private  lateinit var heatMapOverlay : TileOverlay
    private var contact : Contact? = null

    private var contactInterests : ArrayList<Interest>? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var locationAtStart : LatLng? = null

    private var markerList : ArrayList<Marker> = ArrayList()
    private val client: OkHttpClient = OkHttpClient()
    private var placesList: ConcurrentHashMap<String, Place> = ConcurrentHashMap()
    private val moshi: Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
    private val resultsJsonAdapter: JsonAdapter<NearbySearchRequestResults> = moshi.adapter(NearbySearchRequestResults::class.java)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_view)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // Recover passed data
        val data = intent.extras
        contact  = data?.getParcelable("contact") // TODO: Make sure this is the right key string.
        contactInterests = contact?.interests

        title = contact?.contactName

        //Check and ask for location permission
        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
        }
        else
        {
            val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)

            val shouldProvideRationale2 = ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)

            if (shouldProvideRationale || shouldProvideRationale2)
            {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(R.string.title_location_permission)
                builder.setMessage(R.string.text_location_permission)
                builder.setPositiveButton(R.string.ok){_, _ ->
                    ActivityCompat.requestPermissions(this@MapViewActivity,
                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                        1027)
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
            else
            {
                ActivityCompat.requestPermissions(this@MapViewActivity,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    1027)
            }
        }




    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val heatMapTileOverlay = BuildHeatMap.getHetMapTileOverlay()
        if (heatMapTileOverlay != null) {
            mMap.addTileOverlay(heatMapTileOverlay) // Get heatmap overlay data and apply it to the map.
        }

        getLatestLocation() // Get latest location, center camera on it.

        mMap.setInfoWindowAdapter(GiftAdvisorInfoWindowsAdapter(this)) //Set InfowindowAdapter to modify Info Window content presentation.

        // Define long click behavior on map to: Search for nearbly places around clicked position.
        mMap.setOnMapLongClickListener {
            markerList.forEach {
                it.remove()
            }
            markerList.clear()
            getNearbyPlaces(it)

        }

        setInfoWindowLongClickBehavior() // Define long click behavior on InfoWindow to: Open place on Google Maps app (or web).
    }

    private fun setInfoWindowLongClickBehavior() {
        mMap.setOnInfoWindowLongClickListener {
            if ((it.tag as Place).url != null) {

                // If url was requested before, use that to open in external app.
                Handler(Looper.getMainLooper()).post {
                    val gMapIntent = Intent(Intent.ACTION_VIEW, Uri.parse((it.tag as Place).url))
                    this@MapViewActivity.startActivity(gMapIntent)
                }
            } else {

                // If url is missing, make api request and process response.
                val request: Request = Request.Builder()
                    .url(
                        "https://maps.googleapis.com/maps/api/place/details/json?placeid=${(it.tag as Place).reference}&fields=url&key=${getString(
                            R.string.google_maps_key
                        )}"
                    )
                    .build()
                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        val responseString = response.body()?.source()?.readUtf8()
                        val jsonReader = JSONObject(responseString)
                        Handler(Looper.getMainLooper()).post {
                            (it.tag as Place).url = jsonReader.getJSONObject("result").getString("url")

                            val gMapIntent = Intent(Intent.ACTION_VIEW, Uri.parse((it.tag as Place).url))
                            this@MapViewActivity.startActivity(gMapIntent)
                        }
                    }

                })
            }


        }
    }

    private fun getLatestLocation() {

        // Persmission check
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {

            // This will set the camera to the latest known location of the user if it is available.
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation
                .addOnSuccessListener {
                    if (it != null) {
                        locationAtStart = LatLng(it.latitude, it.longitude)
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationAtStart, 12.0F))
                    }
                }
        }
    }

    private fun getNearbyPlaces(center : LatLng) {
        contactInterests?.forEach {

            // If interest is set to be shown on map, send nearby plaves api request around center.
            if (it.showOnMap) {
                val request: Request = Request.Builder()
                    .url(
                        "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${center.latitude},${center.longitude}&radius=200&type=store&keyword=${it.keyword}&key=${getString(
                            R.string.google_maps_key
                        )}"
                    )
                    .build()

                // Enque places all api requests (separate for each interest) on a different thread.
                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        resultsJsonAdapter
                            .fromJson(
                                response
                                    .body()
                                    ?.source()
                                    !!.readUtf8()
                            )
                            ?.results
                            ?.forEach {
                                if (placesList[it.reference] != null) {
                                    it.url = placesList[it.reference]?.url
                                } else {
                                    placesList.put(it.reference, it)
                                }
                                Handler(Looper.getMainLooper()).post {
                                    val marker = mMap.addMarker(
                                        MarkerOptions()
                                            .position(
                                                LatLng(it.geometry.location.lat, it.geometry.location.lng)
                                            )
                                            .title(it.name)
                                            .snippet(it.vicinity)
                                    )

                                    marker.tag = it
                                    markerList.add(marker)
                                }

                            }
                    }

                })
            }
        }
    }
}
