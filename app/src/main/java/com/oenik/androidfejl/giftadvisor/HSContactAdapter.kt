package com.oenik.androidfejl.giftadvisor

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.maps.MapView
import org.threeten.bp.LocalDate
import org.threeten.bp.temporal.ChronoUnit
import java.lang.Exception

class HSContactAdapter (private val contacts: ArrayList<Contact>) :
    RecyclerView.Adapter<HSContactAdapter.HSViewHolder>(){

    class HSViewHolder(val linearLayout: LinearLayout) : RecyclerView.ViewHolder(linearLayout)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HSViewHolder {
        val contactView = LayoutInflater.from(parent.context)
            .inflate(R.layout.hs_recyclerview_item,parent,false) as LinearLayout

        return HSViewHolder(contactView)
    }

    override fun onBindViewHolder(holder: HSViewHolder, position: Int) {

        // gets all 3 TextView children in hs_recyclerview_item.xml
        val nameTextView = holder.linearLayout.findViewById<TextView>(R.id.name)
        val birthdayTextView = holder.linearLayout.findViewById<TextView>(R.id.birthday)
        val ageTextView = holder.linearLayout.findViewById<TextView>(R.id.age)

        // reference to the current contact
        val curr = contacts[position]

        // setting the text property of each TextView
        nameTextView.text = curr.contactName
        birthdayTextView.text = Contact.getBirthdayString(curr)
        ageTextView.text = Contact.getAgeInDaysString(curr)

        holder.linearLayout.setOnClickListener{
            view ->
            val context = view.context
            val intent = Intent(context, MapViewActivity::class.java)
            intent.putExtra("contact", curr)
            context.startActivity(intent)
        }

        holder.linearLayout.setOnLongClickListener{
            view->
            val context = view.context
            val intent = Intent(context, EditFriendActivity::class.java)

            intent.putExtra("contact", curr)
            context.startActivity(intent)
            true
        }


    }

    override fun getItemCount(): Int {
        return contacts.size
    }












}