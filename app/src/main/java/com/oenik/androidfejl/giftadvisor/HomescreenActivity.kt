package com.oenik.androidfejl.giftadvisor

import android.Manifest
import android.app.Activity
import android.app.ActivityManager
import android.support.v7.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.ActivityCompat
import android.view.View
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ListView
import com.google.android.gms.maps.MapView
import com.jakewharton.threetenabp.AndroidThreeTen
import com.oenik.androidfejl.giftadvisor.services.TrackRouteService

class HomescreenActivity : Activity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var contacts : ArrayList<Contact>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homescreen)

        checkLocationPermissions()

        AndroidThreeTen.init(this)
    }

    override fun onStart() {
        super.onStart()

        contacts = loadContacts()
        initRecyclerView()
        initFloatingActionButton()

    }

    override fun onResume() {
        super.onResume()
        contacts = loadContacts()
        initRecyclerView()
    }

    private fun loadContacts() : ArrayList<Contact>{
        return DataBaseHadndler.getInstance(applicationContext).getAllContacts()
    }
    private fun initRecyclerView() {
        viewManager = LinearLayoutManager(this)
        viewAdapter = HSContactAdapter(contacts)

        recyclerView = findViewById<RecyclerView>(R.id.hs_recyclerview).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter
            adapter = viewAdapter
        }




    }
    private fun initFloatingActionButton(){
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener{view ->
            val intent = Intent(view.context, ContactsActivity::class.java)
            startActivity(intent)
        }
    }


    private fun checkLocationPermissions() {
        val permissionCheck1 = ActivityCompat.checkSelfPermission(this,
            android.Manifest.permission.ACCESS_FINE_LOCATION)

        val permissionCheck2 = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)

        if (permissionCheck1 == PackageManager.PERMISSION_GRANTED && permissionCheck2 == PackageManager.PERMISSION_GRANTED)
        {//Permissions are granted
            serviceStart()
        }else
        {//Request permissions
            requestPermissions()
        }
    }

    private fun requestPermissions(){
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
            android.Manifest.permission.ACCESS_FINE_LOCATION)

        val shouldProvideRationale2 = ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)

        if (shouldProvideRationale || shouldProvideRationale2)
        {

            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.title_location_permission)
            builder.setMessage(R.string.text_location_permission)
            builder.setPositiveButton(R.string.ok){_, _ ->
                ActivityCompat.requestPermissions(this@HomescreenActivity,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    1027)
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
                    }
        else
        {
            ActivityCompat.requestPermissions(this@HomescreenActivity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                1027)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        if (requestCode == 1027)
        {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED )
            {
               serviceStart()
            }
            else
            {
                // Permission denied.
                showSnackbar(R.string.text_location_permission,
                    android.R.string.ok, View.OnClickListener {
                        // Request permission
                        ActivityCompat.requestPermissions(this@HomescreenActivity,
                            arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                            1027)
                    })
            }
        }
    }

    private fun showSnackbar(mainTextStringId:Int, actionStringId:Int, listener: View.OnClickListener) {
        Snackbar.make(
            findViewById(android.R.id.content), getString(mainTextStringId),
            Snackbar.LENGTH_INDEFINITE
        ).setAction(getString(actionStringId), listener).show()

    }

    private fun serviceStart() {
        val serviceClass = TrackRouteService::class.java

            val intent = Intent(this,serviceClass)
            startService(intent)
    }
}
