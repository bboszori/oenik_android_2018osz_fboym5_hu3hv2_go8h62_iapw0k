package com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities

import android.app.Activity
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.oenik.androidfejl.giftadvisor.R

class GiftAdvisorInfoWindowsAdapter(val context : Activity) : GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(marker: Marker?): View? {
        val view = context.layoutInflater.inflate(R.layout.gmaps_info_window,null)

        val tvTitle = view.findViewById<TextView>(R.id.tv_title)
        val tvRating = view.findViewById<TextView>(R.id.tv_rating)
        val tvVicinity = view.findViewById<TextView>(R.id.tv_vicinity)
        //val tvInfo = view.findViewById<TextView>(R.id.tv_info)

        tvTitle.text = marker?.title
        tvRating.text = (marker?.tag as Place).rating.toString()
        tvVicinity.text = marker.snippet
        return view
    }

    override fun getInfoWindow(p0: Marker?): View? {
        return  null
    }
}