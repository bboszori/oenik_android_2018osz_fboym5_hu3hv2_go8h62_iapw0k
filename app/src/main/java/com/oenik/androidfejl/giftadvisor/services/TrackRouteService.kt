package com.oenik.androidfejl.giftadvisor.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.app.NotificationCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.oenik.androidfejl.giftadvisor.HomescreenActivity


import com.oenik.androidfejl.giftadvisor.R
import com.oenik.androidfejl.giftadvisor.MapViewActivityUtilities.BuildHeatMap
import kotlin.collections.ArrayList

class TrackRouteService: Service(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private lateinit var locationClient: GoogleApiClient
    private var locationRequest = LocationRequest()
    private var lastLocation: Location? = null
    //private var locations = ArrayList<String>()
    //private var index = 0
    //private var isFull = false
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var locationCallback: LocationCallback = object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return

            if (locationResult.locations.isNotEmpty()){
                //The new location
                lastLocation = locationResult.lastLocation

                if (lastLocation != null){
                    saveLocationToFile(lastLocation?.latitude.toString(), lastLocation?.longitude.toString())
                }
            }
        }
    }

    private fun saveLocationToFile(latitude: String, longitude: String) {
        /*locations.add(index,"$latitude,$longitude\n")

        var loc = ""

        if (isFull){
            for (i in 0..(LOCATION_NUMBER-1)){
                loc+=locations[i]
            }
        }else{
            for (i in 0..(index)) {
                loc+=locations[i]
            }
        }
*/
        try {
            synchronized(BuildHeatMap.lockObject) {
                val loc = "$latitude,$longitude\n"
                val filename = "locations"
                val fos = openFileOutput(filename, Context.MODE_APPEND)

                fos.write(loc.toByteArray())
                fos.close()
            }

        }catch (e:Exception){
            //Error handling?
        }
/*
        index++

        if (index == LOCATION_NUMBER) {
            index = 0
            isFull = true
        }
*/
    }

    override fun onCreate() {
        super.onCreate()

        BuildHeatMap.heatMapDataFilePath = applicationContext.filesDir.path + "/locations"

        locationClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        locationRequest.interval = INTERVAL
        locationRequest.fastestInterval = FASTEST_INTERVAL
        locationRequest.maxWaitTime = MAX_WAIT_TIME

        val priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        //PRIORITY_BALANCED_POWER_ACCURACY, PRIORITY_LOW_POWER, PRIORITY_NO_POWER are the other priority modes

        locationRequest.priority = priority
        locationClient.connect()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        val channelId = createNotificationChannel("my_service")


        val intent = Intent(this, HomescreenActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent = PendingIntent.getActivity(this,0,intent,0)

        val notification = NotificationCompat.Builder(this, channelId)
            .setContentTitle(getString(R.string.track))
            .setContentText(getString(R.string.tracking))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            // Set the intent that will fire when the user taps the notification
            .setContentIntent(pendingIntent)
            .build()

        startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    private fun createNotificationChannel(channelId: String): String{
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance).apply {
            description = descriptionText
        }
            // Register the channel with the system
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.createNotificationChannel(channel)
        }
        return channelId
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        return Service.START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onConnected(p0: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        fusedLocationClient.requestLocationUpdates(locationRequest,locationCallback, Looper.myLooper())
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    companion object {

        const val INTERVAL:Long  = 60 * 1000
        const val FASTEST_INTERVAL:Long = 60 * 1000
        const val MAX_WAIT_TIME:Long = 10 * 60 * 1000
        const val ONGOING_NOTIFICATION_ID = 1
        //const val LOCATION_NUMBER = 100
    }
}