package com.oenik.androidfejl.giftadvisor

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.oenik.androidfejl.giftadvisor.databinding.EfRecyclerviewItemBinding

class EFContactAdapter (private val interests: ArrayList<Interest>) :
    RecyclerView.Adapter<EFContactAdapter.EFViewHolder>() {

    class EFViewHolder( private val binding : EfRecyclerviewItemBinding)
          : RecyclerView.ViewHolder(binding.root){


        fun bind(interest: Interest){
            binding.interest=interest
            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EFViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val interestBinding  =
                            EfRecyclerviewItemBinding.inflate(layoutInflater,parent, false)
        return EFViewHolder(interestBinding)

    }

    override fun onBindViewHolder(holder: EFViewHolder, position: Int) {
        val interest : Interest = interests[position]
        holder.bind(interest)
    }

    override fun getItemCount(): Int {
        return interests.size
    }
}